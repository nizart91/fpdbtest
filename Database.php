<?php

namespace FpDbTest;

use Exception;
use InvalidArgumentException;
use mysqli;

class Database implements DatabaseInterface
{
    private mysqli $mysqli;

    public function __construct(mysqli $mysqli)
    {
        $this->mysqli = $mysqli;
    }

    public function buildQuery(string $query, array $args = []): string
    {
//        throw new Exception();
        $query = preg_replace_callback('/\?([dfas#]|)/', function ($matches) use (&$args) {
            switch ($matches[1]) {
                case 'd': // конвертация в целое число
                case 'f': // конвертация в число с плавающей точкой
                    return $this->prepareValue(array_shift($args), $matches[1]);
                case 'a': // массив значений
                    $arr = array_shift($args);
                    if (!is_array($arr)) {
                        throw new InvalidArgumentException('Parameter for ?a specifier must be an array');
                    }
                    return implode(', ',
                        (array_keys($arr) !== range(0, count($arr) - 1)) ?
                            array_map(fn($k, $v) => $this->prepareIdentifier($k) . ' = ' . $this->prepareValue($v), array_keys($arr), array_values($arr)) :
                            array_map([$this, 'prepareValue'], $arr)
                    );
                case '#': // идентификатор или массив идентификаторов
                    $arr = array_shift($args);
                    if (!is_array($arr)) {
                        $arr = [$arr];
                    }
                    return implode(', ', array_map([$this, 'prepareIdentifier'], $arr));
                default:
                    return $this->prepareValue(array_shift($args));
            }
        }, $query);

        return preg_replace_callback('/\{([^{}]+)\}/', function ($matches) {
            return str_contains($matches[1], $this->skip()) ? '' : $matches[1];
        }, $query);
    }

    /**
     * Экранирование идентификаторов
     *
     * @param $identifier
     * @return string
     */
    protected function prepareIdentifier($identifier): string
    {
        return "`" . $this->mysqli->real_escape_string($identifier) . "`";
    }

    /**
     * Преобразование значения
     *
     * @param $value
     * @param string|null $specifier
     * @return string|int|float
     * @throws InvalidArgumentException
     */
    protected function prepareValue($value, ?string $specifier = null): string|int|float
    {
        return match (true) {
            $value === $this->skip() => $value,
            is_null($value) => 'NULL',
            $specifier === 'd', is_bool($value), is_int($value) => (int)$value,
            $specifier === 'f', is_float($value) => (float)$value,
            is_string($value) => "'" . $this->mysqli->real_escape_string($value) . "'",
            default => throw new InvalidArgumentException('Invalid parameter value'),
        };
    }

    /**
     * @return string
     */
    public function skip(): string
    {
        return 'skip_current_condition';
    }
}
